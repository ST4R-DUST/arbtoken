import { BlackButtonS } from "./styles";
import { ButtonProps } from "../../types";

export const BlackButton = ({
  color,
  fixedWidth,
  children,
  onClick,
}: ButtonProps) => (
  <BlackButtonS color={color} fixedWidth={fixedWidth} onClick={onClick}>
    {children}
  </BlackButtonS>
);
