import styled from "styled-components";

export const StyledButton = styled("button")<any>`
  background: ${(p) => p.color || "#0c84e4"};
  color: ${(p) => (p.color ? "#fff" : "#fff")};
  font-size: 1rem;
  font-weight: 700;
  width: 100%;
  border: 1px solid #0c84e4;
  border-radius: 4px;
  padding: 13px 0;
  cursor: pointer;
  margin-top: 0.625rem;
  max-width: 180px;
  transition: all 0.3s ease-in-out;
  box-shadow: 10px 10px 11px 0px rgba(0,0,0,0.26);
-webkit-box-shadow: 10px 10px 11px 0px rgba(0,0,0,0.26);
-moz-box-shadow: 10px 10px 11px 0px rgba(0,0,0,0.26);

  &:hover,
  &:active,
  &:focus {
    color: #0c84e4;
    border: 1px solid rgb(255,255,255);
    background-color: rgb(255,255,255);
  }
`;