import { lazy } from "react";
import {
  BackgroundContainer
} from "./styles";


const Container = lazy(() => import("../../common/Container"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
const TokenomicsBlock = lazy(() => import("../../components/Home/TokenomicsBlock"));
const AboutBlock = lazy(() => import ("../../components/Home/AboutBlock"));
const IntroBlock = lazy(() => import ("../../components/Home/IntroBlock"));
const RoadmapBlock = lazy(() => import ("../../components/Home/RoadmapBlock"));
const ChartBlock = lazy(() => import ("../../components/Home/ChartBlock"));


const Home = () => {
  return (
    <>
    <BackgroundContainer>
        <div style={{
              background: "linear-gradient(90deg, rgba(0,0,0,0.3900910706079307) 0%, rgba(0,0,0,0.8858893899356618) 100%)",
              borderBottomLeftRadius:"81px",
              borderBottomRightRadius:"81px",
              boxShadow:"12px 12px 12px 6px rgba(0,0,0,0.1)"
            }}>
        <Container>
      
        
        <IntroBlock/>
        </Container>
        </div>
        <Container>
        <TokenomicsBlock/>
        </Container>
        <div style={{

          }}>
        <Container>
        <RoadmapBlock/>
        </Container>
        </div>
        <ChartBlock/>

        <div style={{

          }}>
        <Container>
        
        <AboutBlock/>
        
        </Container>
        
        </div>
        
        <ScrollToTop />
    </BackgroundContainer>
    </>
  );
};
export default Home;
