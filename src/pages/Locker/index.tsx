import { lazy } from "react";
import {
  BackgroundContainer
} from "./styles";

const Container = lazy(() => import("../../common/LockerContainer"));
const ScrollToTop = lazy(() => import("../../common/ScrollToTop"));
const LockerBlock = lazy(() => import ("../../components/Locker/LockerBlock"));


const Locker = () => {
  return (
  <BackgroundContainer>
    <Container>
      
      <ScrollToTop />
      <LockerBlock/>
      
    </Container>
  </BackgroundContainer>
  );
};
/*<Contact
title={ContactContent.title}
content={ContactContent.text}
id="contact"
/>*/
export default Locker;
