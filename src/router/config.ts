const routes = [
  {
    path: ["/", "/home"],
    exact: true,
    component: "Home",
  },
  /*{
    path: ["/locker"],
    exact: true,
    component: "Locker",
  },*/
];

export default routes;
