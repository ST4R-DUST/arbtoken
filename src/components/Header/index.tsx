import { useState } from "react";
import { Link, useLocation  } from "react-router-dom";
import { Row, Col, Drawer } from "antd";
import Container from "../../common/Container";
import { SvgIcon } from "../../common/SvgIcon";
import { Button } from "../../common/Buttons/Button";
import { BlackButton } from "../../common/Buttons/BlackButton";
import { useWeb3React } from "@web3-react/core"
import { injected } from "../../MMConnect"
import "antd/dist/antd.css";
import "./index.css";
//import { useWeb3React } from "@web3-react/core"
//import { injected } from "../../MMConnect"
import {
  HeaderSection,
  LogoContainer,
  Burger,
  NotHidden,
  Menu,
  CustomNavLinkSmall,
  Label,
  Outline,
  Span,
} from "./styles";

function Header(){
  const [visible, setVisibility] = useState(false);
  
  const location = useLocation()
  var Locker = false;
  if(location.pathname === '/Locker')
  {
    Locker = true
  }

  //Web3 Stuff
  const { active, activate, deactivate } = useWeb3React()
  async function connect() {
    try {
      await activate(injected)
    } catch (ex) {
      console.log(ex)
    }
  }

  async function disconnect() {
    try {
      deactivate()
    } catch (ex) {
      console.log(ex)
    }
  }
 /* const { active, account, library, connector, activate, deactivate } = useWeb3React()
  <Button onClick={connect} >Connect to MetaMask</Button>
      {active ? <p>Connected with {account}</p> : <p>no funca la wea</p>}
      <Button onClick={disconnect} >Disconnect</Button>
  */

  const showDrawer = () => {
    setVisibility(!visible);
  };

  const onClose = () => {
    setVisibility(!visible);
  };

  const MenuItem = () => {
    const scrollTo = (id: string) => {
      const element = document.getElementById(id) as HTMLDivElement;
      element.scrollIntoView({
        behavior: "smooth",
      });
      setVisibility(false);
      
    };
    return (
      <>
          <CustomNavLinkSmall >
            <a href="https://arbee.info">
              <Span>arbee.info</Span>
            </a>
          </CustomNavLinkSmall>
        
        
          <CustomNavLinkSmall >
            <a style={{textDecoration:'none'}} href="https://t.me/arbystoken">
            <Span>Telegram</Span>
            </a>
          </CustomNavLinkSmall>
        
        <CustomNavLinkSmall onClick={() => scrollTo("tokenomics")}>
          <Span style={{color:"#fff"}}>Tokenomics</Span>
        </CustomNavLinkSmall>
        <CustomNavLinkSmall onClick={() => scrollTo("roadmap")}>
          <Span style={{color:"#fff"}}>Roadmap</Span>
        </CustomNavLinkSmall>
        
        

        <CustomNavLinkSmall
          style={{ width: "108px", zIndex:6}}
        >
          <Span>
          <a href="https://app.sushi.com/swap?&outputCurrency=0x86a1012d437bbff84fbdf62569d12d4fd3396f8c">
            <Button>Buy <SvgIcon src="av.svg" width="24px" height="24px" /></Button>
          </a>
          </Span>
        </CustomNavLinkSmall>
      </>
    );
  };

const RedirectItem = () => {
    
  return (
    <>
      { Locker ? 
      <>
      
      <CustomNavLinkSmall
        style={{ width: "110px"}}
      >
        <Span>
        <Link to="/">
          <BlackButton >Home </BlackButton>
        </Link>
        </Span>
      </CustomNavLinkSmall> 
      </>
      :
      <CustomNavLinkSmall
        style={{ width: "110px" }}
      >
        <Span>
        <Link to="/Locker">
          <BlackButton>Locker</BlackButton>
        </Link>
        </Span>
      </CustomNavLinkSmall>}

      
      

      
    </>
  );
  }

  return (

    
    <HeaderSection style={{zIndex:5}}>
      <Container>
        <Row justify="space-between" >
          <LogoContainer to="/" aria-label="homepage" >
            <SvgIcon src="logoalt.png" width="203px" height="64px" /> 
          </LogoContainer>
          <Row >
            
          { Locker ?  <CustomNavLinkSmall
              style={{ width: "120px"}}
            >

                {active ? <Button onClick={disconnect} >Disconnect</Button> : <Button onClick={connect} >Connect Wallet</Button>}
            </CustomNavLinkSmall>  : <></>}
            
            <NotHidden>
            { Locker ?  <></> : <MenuItem />}
            <RedirectItem/>
            </NotHidden>
          </Row>
          <Burger onClick={showDrawer}>
            <Outline />
          </Burger>
        </Row>
        <Drawer closable={false} visible={visible} onClose={onClose}>
          <Col style={{ marginBottom: "2.5rem" }}>
            <Label onClick={onClose}>
              <Col span={12}>
                <Menu>Menu</Menu>
              </Col>
              <Col span={12}>
                <Outline />
              </Col>
            </Label>
          </Col>
          <Col span={12}>
            <RedirectItem/>
            
            { Locker ?  <></> : <MenuItem />}
          </Col>
        </Drawer>
      </Container>
    </HeaderSection>
  );
};

export default Header;
