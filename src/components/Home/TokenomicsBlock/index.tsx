import { Row, Col } from "antd";
//import { SvgIcon } from "../../../common/SvgIcon";
import { Fade } from "react-awesome-reveal";
import {
  LeftContentSection,
  ContentWrapper,
  Content,
} from "./styles";

const LeftContentBlock = () => {
  return (
    <LeftContentSection id={"tokenomics"}>
      <Fade direction="left">
        <Row justify="space-between" align="middle" >
          <Col lg={11} md={11} sm={11} xs={24}>
            <ContentWrapper>
              <h6 style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)"}}>Tokenomics</h6>
              
              
            </ContentWrapper>
          </Col>
          <Col lg={10} md={24} sm={24} xs={24}>
            <Content>
              <b>
              <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                ⚫ Low Supply (100K TOKENS)
              </p>
              <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                ⚫ Devs overpaid for tokens on launch
              </p>
              <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                ⚫ Liquidity incentivized through yielding partnerships and features unlocked through liquidity provisions
              </p>
              <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                ⚫ Transferrable with no tax
              </p>
              </b>
              *No slippage required.
            </Content>
          </Col>
        </Row>
      </Fade>
    </LeftContentSection>
  );
};

export default (LeftContentBlock);
