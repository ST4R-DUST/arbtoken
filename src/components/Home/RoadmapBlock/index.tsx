import { Row, Col, Timeline } from "antd";
import { Fade } from "react-awesome-reveal";
import {
  RightBlockContainer,
  Content,
} from "./styles";

const RightContentBlock = () => {
  return (
    <RightBlockContainer id={"roadmap"}>
      <Fade direction="right">
        <Row justify="space-between" align="middle" style={{paddingBottom:"64px"}}>
          <Col lg={11} md={11} sm={11} xs={24}>
            <h6 style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)"}}>Roadmap</h6>
          </Col>
          <Col lg={11} md={11} sm={11} xs={24}>
            <img style={{borderRadius:"1024px"}} src={"/img/roadmap.png"} alt="Avatar" width="100%" height="100%"/>
          </Col>
        </Row>
        <Row justify="space-between" align="middle">
          <Col lg={10} md={24} sm={24} xs={24}>
            <Content>
              <Timeline>
                <Timeline.Item color="white">
                  <p>
                    <span style={{backgroundColor:"#fff",color:"#000", padding:"15px", borderRadius:"50px", border:"4px solid #000", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                      $ARBYS Launch ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      Listed on CoinMarketCap ✅ (In the first ever batch of arbitrum tokens listed)
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p>
                    <span style={{backgroundColor:"#fff",color:"#000", padding:"15px", borderRadius:"50px", border:"4px solid #000", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                      Listed on Coingecko ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      Sushiswap Logo applied for ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      <b>First ever</b> Portfolio Tracker on Arbitrum ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p>
                    <span style={{backgroundColor:"#fff",color:"#000", padding:"15px", borderRadius:"50px", border:"4px solid #000", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                      Hit 350 Holders ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      Start a Yield Farm with TheHoneyPot ✅
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      Recognize the problems with fair launch liquidity and take measures to increase available liquidity for the project <img src={`/img/svg/loading.gif`} alt={'loading.gif'} width={'24px'} height={'24px'}/>
                    </span>
                  </p>
                </Timeline.Item>
              </Timeline>
            </Content>
          </Col>
          <Col lg={10} md={24} sm={24} xs={24}>
            <Content>
              <Timeline>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Pursue NFT and Yielding partnerships with other projects <img src={`/img/svg/loading.gif`} alt={'loading.gif'} width={'24px'} height={'24px'}/>
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Overhaul a new and improved charting solution to be combined with Arbee.info
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                      Reach at least 1000 Holders
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Hit and mantain an 8 figure Marketcap
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Airdrop NFTs to Loyal Holders
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Introduce new features and quality of life changes
                    </span>
                  </p>
                </Timeline.Item>
                <Timeline.Item color="white">
                  <p style={{backgroundColor:"#fff",color:"#000", padding:"15px", border:"4px solid #000", borderRadius:"50px", boxShadow:"0px 7px 20px 0px rgba(0,0,0,0.5)"}}>
                    <span >
                    Get listed on a major CEX
                    </span>
                  </p>
                </Timeline.Item>
                
                
                
              </Timeline>
            </Content>
          </Col>
        </Row>
      </Fade>
    </RightBlockContainer>
  );
};

export default RightContentBlock;
