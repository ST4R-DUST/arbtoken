import { Row, Col} from "antd";
import { Fade } from "react-awesome-reveal";
import {
  LeftContentSection,
  ContentWrapper,
  CardDiv,
} from "./styles";

const LeftContentBlock = () => {
  return (
    <LeftContentSection id={"about"}>
      <Fade direction="left">
        <Row justify="space-between" align="middle">
          <Col lg={11} md={11} sm={11} xs={24}>
            <ContentWrapper>
              <h6 style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)"}}>The Team</h6>
            </ContentWrapper>
          </Col>
          
        </Row>
            <Row gutter={32}>
              <Col style={{paddingBottom:"24px"}} lg={8} md={11} sm={11} xs={24}>
                  <CardDiv>
                  <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                      <img src={`/img/svg/BN.jpg`} alt={'BN.jpg'} style={{borderRadius:"120px"}} width={'51%'} height={'51%'}/>
                  </p>
                  <p style={{ textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                    <b>Bruh Network</b>
                  </p>
                  <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                    Manager and Marketing hype man
                  </p>
                  </CardDiv>
              </Col>
              <Col style={{paddingBottom:"24px"}} lg={8} md={11} sm={11} xs={24}>
                <CardDiv>
                <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                    <img src={`/img/svg/LJ.jpg`} alt={'LJ.jpg'} style={{borderRadius:"120px"}} width={'50%'} height={'50%'}/>
                </p>
                <p style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                  <b>Lord Johnson</b>
                </p>
                <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                  Contract deployer and Backend Locker Dev
                </p>
                </CardDiv>
              </Col>
              <Col style={{paddingBottom:"24px"}} lg={8} md={11} sm={11} xs={24}>
                <CardDiv>
                  <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                      <img src={`/img/svg/PF.jpg`} alt={'PF.jpg'} style={{borderRadius:"120px"}} width={'52%'} height={'52%'}/>
                  </p>
                  <p style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                    <b>Printf</b>
                  </p>
                  <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                    Locker Code Provider and Advisor
                  </p>
                </CardDiv>
              </Col>
              <Col style={{paddingBottom:"24px"}} lg={8} md={11} sm={11} xs={24}>
                <CardDiv>
                  <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                      <img src={`/img/svg/st.jpg`} alt={'PF.jpg'} style={{borderRadius:"120px"}} width={'52%'} height={'52%'}/>
                  </p>
                  <p style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                    <b>ST4RDUST</b>
                  </p>
                  <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                    Frontend Locker Developer and Webdev
                  </p>
                </CardDiv>
              </Col>
              <Col style={{paddingBottom:"24px"}} lg={8} md={11} sm={11} xs={24}>
                <CardDiv>
                  <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                      <img src={`/img/svg/neat.jpg`} alt={'PF.jpg'} style={{borderRadius:"120px"}} width={'52%'} height={'52%'}/>
                  </p>
                  <p style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                    <b>Neat</b>
                  </p>
                  <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                    Arbee.Info Creator / Utility Optimizer
                  </p>
                </CardDiv>
              </Col>
              <Col lg={8} md={11} sm={11} xs={24}>
                <CardDiv>
                  <p style={{display:'flex', justifyContent:'center', padding: '60px 0px 0px'}}>
                      <img src={`/img/svg/ark.jpg`} alt={'PF.jpg'} style={{borderRadius:"120px"}} width={'52%'} height={'52%'}/>
                  </p>
                  <p style={{textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)", display:'flex', justifyContent:'center', color:'#fff', fontSize:32}}>
                    <b>Arkwave</b>
                  </p>
                  <p style={{display:'flex', justifyContent:'center', color:'#fff', fontSize:16}}>
                    Developer of Arbys Charts V2
                  </p>
                </CardDiv>
              </Col>
          </Row>

      </Fade>
    </LeftContentSection>
  );
};

export default (LeftContentBlock);
