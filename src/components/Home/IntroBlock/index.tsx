import { Row, Col } from "antd";
import { SvgIcon } from "../../../common/SvgIcon";
import { Button } from "../../../common/Buttons/Button";
import { Fade } from "react-awesome-reveal";
import {
  RightBlockContainer,
  Content,
  ContentWrapper,
} from "./styles";
/*

*/
const RightBlock = () => {
  return (
    
    <RightBlockContainer id="intro">
      <Fade direction="right">
        <Row justify="space-between" align="middle">
          <Col lg={11} md={11} sm={11} xs={24}>
            <ContentWrapper>
              <h6 style={{color:"#fff", textShadow:"2px 4px 8px rgba(255, 255, 255, 0.5)"}}>$ARBYS</h6>
              <Content style={{color:"#fff"}}>MEATS, UTILITIES, NFTs, MEMES</Content>
                
                <Row justify="space-between" align="middle" style={{zIndex:6}}>
                  <Col lg={12} md={24} sm={12} xs={11}>
                    <a href="https://app.sushi.com/swap?inputCurrency=ETH&outputCurrency=0x86A1012d437BBFf84fbDF62569D12d4FD3396F8c">
                      <Button>Buy <SvgIcon src="av.svg" width="24px" height="24px" /></Button>
                    </a>
                  </Col>
                  <Col lg={12} md={12} sm={12} xs={11}>
                    <a href="https://t.me/arbystoken">
                    <Button>Telegram <SvgIcon src="tg.png" width="24px" height="24px" /></Button>
                    </a>
                  </Col>
                </Row>
            </ContentWrapper>
          </Col>
          <Col lg={11} md={11} sm={12} xs={24}>
            <SvgIcon src={"logo.svg"} width="100%" height="100%" />
          </Col>
        </Row>
      </Fade>
    </RightBlockContainer>
  );
};

export default RightBlock;
